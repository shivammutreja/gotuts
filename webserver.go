package main

import ("fmt"
	"net/http")


func index_handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Index")
}

func about_handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "About")
}

func main() {
	http.HandleFunc("/", index_handler)
	http.HandleFunc("/about/", about_handler)
	http.ListenAndServe(":9001", nil)
}

