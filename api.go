package main

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"strconv"
)

type Person struct {
	Id        string   `json:"id"`
	Firstname string   `json:"first_name"`
	Lastname  string   `json:"last_name"`
	Address   *Address `json:"address"`
}

type Address struct {
	City  string `json:"city"`
	State string `json:"state"`
}

var people []Person

func GetPeople(w http.ResponseWriter, r *http.Request) {
	json.NewEncoder(w).Encode(people)
}

func main() {
	router := mux.NewRouter()
	nums := []int{2, 3, 4}
	for _, num := range nums {
		people = append(people, Person{Id: strconv.Itoa(num), Firstname: "John", Lastname: "Doe", Address: &Address{City: "City X", State: "State X"}})
	}
	//people = append(people, Person{Id: "1", Firstname: "John", Lastname: "Doe", Address: &Address{City: "City X", State: "State X"}})
	//people = append(people, Person{Id: "2", Firstname: "John", Lastname: "Nadoe", Address: &Address{City: "City Y", State: "State Y"}})

	router.HandleFunc("/people", GetPeople).Methods("GET")
	log.Fatal(http.ListenAndServe(":9002", router))
}
