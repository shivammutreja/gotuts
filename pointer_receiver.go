package main

import "fmt"

const usixteenbitmax float64 = 65535
const kmh_multiple float64 = 1.609

type car struct {
	gas_pedal      uint16
	brake_pedal    uint16
	steering_wheel uint16
	top_speed_kmh  float64
}

func (c car) kmh() float64 {
	return float64(c.gas_pedal) * (c.top_speed_kmh / usixteenbitmax)
}

func (c car) mph() float64 {
	return float64(c.gas_pedal) * (c.top_speed_kmh / usixteenbitmax / kmh_multiple)
}

func (c *car) new_speed(speed float64) {
	c.top_speed_kmh = speed
}

/* One can achieve the above pointer_receiver method
in a different way as written below.

func newer_speed(c car, speed float64) car {
	c.top_speed_kmh = speed
	return c
}
*/

func main() {
	a_car := car{
		gas_pedal:      50000,
		brake_pedal:    21,
		steering_wheel: 32,
		top_speed_kmh:  225.0}
	fmt.Println(a_car.kmh())
	fmt.Println(a_car.mph())
	a_car.new_speed(500)
	//a_car = newer_speed(a_car, 500)
	fmt.Println(a_car.kmh())
	fmt.Println(a_car.mph())
}
