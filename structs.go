package main

import "fmt"

type car struct {
	gas_pedal uint16
	brake_pedal uint16
	steering_wheel uint16
	top_speed_kmh float64
}

func main() {
	a_car := car{
			gas_pedal: 12,
			brake_pedal: 21,
			steering_wheel: 32,
			top_speed_kmh: 225.0}
	fmt.Println(a_car.top_speed_kmh)
}

