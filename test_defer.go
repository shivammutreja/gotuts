package main

import (
	"encoding/json"
	"fmt"
	"os"
)

/* You’d need to run this code on your local machine */
/* Because, play.golang.org doesn't allow files */
/* Exercise: How to imitate if there is a file? */

type Config struct {
	Proto string
}

func NewFromFile(path string) (c *Config, err error) {
	c = &Config{}

	file, err := os.Open(path)
	if err != nil {
		return
	}
	defer file.Close()

	err = json.NewDecoder(file).Decode(c)
	if err != nil {
		return
	}

	return
}

func main() {
	cfg, err := NewFromFile("/home/madhu/Mnemonics/config.json")

	if err != nil {
		panic(err)
	}

	fmt.Printf("%#v\n", cfg)
}


