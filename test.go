package main

import "fmt"

func add(x,y float32) float32 {
	return x+y
}

func multiple(w1, w2 string) (string, string) {
	return w1, w2
}

func main() {
	var a int = 22
	var b float64 = float64(a)
	x := a
	fmt.Println(a, x, b)
	w1, w2 := "Hey", "there"
	fmt.Println(multiple(w1,w2))
}

